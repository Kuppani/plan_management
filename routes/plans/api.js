const router = require('express').Router();
const Plan = require('../../models/plan');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
//used for image uploading
var multer  = require('multer');
var aws = require('aws-sdk');
var fs = require('fs');

/* Multer set storage location*/
var storage = multer.diskStorage({
	
	filename: function (req, file, cb) {
		   var str = file.originalname;
		   str = str.replace(/\s+/g, '-').toLowerCase();
		   global.poster = Date.now() + '_' + str;
		   cb(null, poster);
	 }
   });

   var upload = multer({ storage: storage });
   aws.config.update({ accessKeyId: 'AKIAIJCZVHKXQKI3ENYQ', secretAccessKey: '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' });
   aws.config.update({region: 'ap-south-1'});

const apiOperation = (req, res,crudMethod,optionsObj)=> {
	const bodyParams = Methods.initializer(req, Plan);
	crudMethod(bodyParams, optionsObj, (err, plan) => {
		console.log('\nplan details', plan);
		res.send(plan);
	})
}

// Here the roo path '/' is  '/api/plans/'
router.get('/', (req, res) => {
	res.send('This is the api route for plan management');
});

router.get('/getallplans', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";
					Plan.selectTable(table);
					Plan.query('plan',(err,plans)=>{
						//The error is already handled in the query method; but the error first approach is used here, so if we wan't to do the error handling
						//here instead of the query function, it will be easier to switch back
						res.send(plans);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_store_data';
					console.log("Table name"+table);
					Plan.selectTable(table);
					apiOperation(req, res, Plan.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/addplan',upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	console.log(token);
	//console.log("params:"+req.body);

	console.log("req value: "+JSON.stringify(req.file));
	//console.log("image:"+req.file.originalname);
	var s3 = new aws.S3();
	var hello=req.file.path;
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					s3.upload({
						"ACL":"public-read",
						  "Bucket": "cadenza-api-documentation-new",
						   "Key": poster,
							 "Body": fs.createReadStream(hello),
							 ContentType : "image/jpeg"
						}, function(err, data) {
						if (err) {
							  console.log("Error uploading data: ", err);
						} else {
							console.log("upload success");
							const imageurl="https://s3-ap-southeast-1.amazonaws.com/cadenza-api-documentation-new/"+poster;
							console.log("imageurl:"+imageurl);
							console.log(decode.storeName);
							const bodyParams = Methods.initializer(req, Plan);
							const putParams = {
								"collectionType":"plan",
								"collectionId": req.body.collectionId,
								"title": req.body.title,
								"description": req.body.des,
								"price": req.body.price,
								"imageURL":imageurl

							}
								
								Plan.createItem(putParams, {
									table: decode.storeName+"_store_data",
									overwrite: false
								}, (err, plan) => {
									console.log('\nplan details', plan);
									res.send(plan);
								})
							}
						});
								
							}
							
						});
						
					}
    else{
        res.send("please send a token");
    }
});

router.put('/updateplan', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{

				const table = decode.storeName+"_store_data";
				Plan.selectTable(table);
				apiOperation(req, res, Plan.updateItem);
				   
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.delete('/deleteplan', (req, res) => {
	var token = req.body.token || req.headers['token'];	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";
					Plan.selectTable(table);
					apiOperation(req, res, Plan.deleteItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

module.exports = router;